﻿using LegoRecognition.Io.File;
using LegoRecognition.Io.Image;
using LegoRecognition.MyImageProcessing;
using System;
using System.Collections.Generic;

namespace LegoRecognition
{
    class Program
    {
        static void Main( string[] args )
        {
            if ( !string.IsNullOrEmpty( args[0] ) )
            {
                string filePath = args[0];

                BmpHandler bmpFile = new BmpHandler();
                Conversion conversion = new Conversion();
                Filter filter = new Filter();

                Image image = bmpFile.Read( filePath );

                image = conversion.Convert24bitGrayscale( image );

                bmpFile.Write( "gray.bmp", image );

                image = filter.Laplace( image );

                bmpFile.Write( "edge.bmp", image );

                image = conversion.Convert24bitBinary( image, 254 );

                bmpFile.Write( "binary.bmp", image );

            }
            else
            {
                Console.WriteLine( "Please specify the BMP file path!" );
            }
        }
    }
}
