﻿using System;

namespace MyImageProcessing
{
	/// <summary>
	/// Description of Point2D.
	/// </summary>
	public class Point2D
	{
		
		private double u;
		
		public double U {
			get { return u; }
			set 
			{ 
				u = value;
				registrated = true;
			}
		}
		
		private double v;
		
		public double V {
			get { return v; }
			set 
			{ 
				v = value;
				registrated = true;
			}
		}
		
		private bool registrated;
		
		public bool Registrated {
			get { return registrated; }
		}
		
		public Point2D(double u, double v)
		{
			this.u = u;
			this.v = v;
			this.registrated = false;
		}
		
	}
}
