﻿using LegoRecognition.Io.Image;
using System.Collections.Generic;

namespace LegoRecognition.MyImageProcessing
{
    public class Filter
    {
        public Image Laplace(Image input)
        {
            Image newImage = new Image();
            newImage.Width = input.Width;
            newImage.Height = input.Height;

            for ( int j = 0; j < input.Height; j++ )
            {
                for ( int i = 0; i < input.Width; i++ )
                {
                    Pixel p = new Pixel();
                    if ( i == 0 || i == input.Width - 1 || j == 0 || j == input.Height - 1 )
                    {
                        p.Intensity = p.Red = p.Green = p.Blue = 0;
                    }
                    else
                    {
                        p.Intensity = p.Red = p.Green = p.Blue =
                            input.Pixels[(i - 1, j - 1)].Intensity +
                            input.Pixels[(i - 1, j)].Intensity +
                            input.Pixels[(i - 1, j + 1)].Intensity +
                            input.Pixels[(i, j - 1)].Intensity +
                            input.Pixels[(i, j)].Intensity +
                            input.Pixels[(i, j + 1)].Intensity +
                            input.Pixels[(i + 1, j - 1)].Intensity +
                            input.Pixels[(i + 1, j)].Intensity +
                            input.Pixels[(i + 1, j + 1)].Intensity;
                    }
                    newImage.Pixels.Add( (i, j), p );
                }
            }

            return newImage;
        }
    }
}
