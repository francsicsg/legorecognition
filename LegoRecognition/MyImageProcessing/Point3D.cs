﻿using System;

namespace MyImageProcessing
{
	/// <summary>
	/// Description of Point3D.
	/// </summary>
	public class Point3D
	{
		private double x;
		
		public double X {
			get { return x; }
			set 
			{ 
				x = value; 
				registrated = true;
			}
		}
		private double y;
		
		public double Y {
			get { return y; }
			set 
			{ 
				y = value; 
				registrated = true;
			}
		}
		private double z;
		
		public double Z {
			get { return z; }
			set 
			{ 
				z = value; 
				registrated = true;
			}
		}
		
		private bool registrated;
		
		public bool Registrated {
			get { return registrated; }
		}
		
		public Point3D(double x, double y, double z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.registrated = false;
		}
	}
}
