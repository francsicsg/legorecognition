﻿using LegoRecognition.Io.Image;
using System;
using System.Collections.Generic;
using System.Text;

namespace LegoRecognition.MyImageProcessing
{
    public class Conversion
    {
        public Image Convert24bitGrayscale(Image input)
        {
            Image newImage = new Image();
            newImage.Width = input.Width;
            newImage.Height = input.Height;
            
            for ( int y = 0; y < input.Height; y++ )
            {
                for ( int x = 0; x < input.Width; x++ )
                {
                    Pixel pixel = new Pixel();
                    pixel.Intensity = pixel.Red = pixel.Green = pixel.Blue = (int) (0.3 * input.Pixels[(x, y)].Red + 0.6 * input.Pixels[(x, y)].Green + 0.1 * input.Pixels[(x, y)].Blue);
                    newImage.Pixels.Add( (x, y), pixel );
                }
            }

            return newImage;
        }

        public Image Convert24bitBinary( Image input, int threshold )
        {
            Image newImage = new Image();
            newImage.Width = input.Width;
            newImage.Height = input.Height;

            for ( int y = 0; y < input.Height; y++ )
            {
                for ( int x = 0; x < input.Width; x++ )
                {
                    Pixel pixel = new Pixel();
                    if ( input.Pixels[(x, y)].Intensity > threshold )
                    {
                        pixel.Intensity = pixel.Red = pixel.Green = pixel.Blue = 255;
                    }
                    else
                    {
                        pixel.Intensity = pixel.Red = pixel.Green = pixel.Blue = 0;
                    }
                    newImage.Pixels.Add( (x, y), pixel );
                }
            }

            return newImage;
        }

    }
}
