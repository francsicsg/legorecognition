﻿using LegoRecognition.Io.Image;
using System;
using System.Collections.Generic;
using System.IO;

namespace LegoRecognition.Io.File
{
    public class BmpHandler
    {
        private int fileSize;
        private int imOffset;
        private int imSize; //with padding bytes
        private int infoSize;
        private int width;
        private int height;
        private int colorDepth;
        private int compression;
        private int padding;

        private byte[] imBlock;
        private byte[] headerBlock;
        
        public BmpHandler()
        {
            //do nothing;
        }

        public BmpHandler( string filePath )
        {
            this.Read( filePath );
        }

        public Io.Image.Image Read(string filePath )
        {
            Io.Image.Image image = new Io.Image.Image();

            byte[] temp = new byte[4];

            FileStream aFile = new FileStream( filePath, FileMode.Open );

            aFile.Seek( 2, SeekOrigin.Begin );
            aFile.Read( temp, 0, 4 );
            this.fileSize = ( temp[3] << 24 ) + ( temp[2] << 16 ) + ( temp[1] << 8 ) + temp[0];

            aFile.Seek( 10, SeekOrigin.Begin );
            aFile.Read( temp, 0, 4 );
            this.imOffset = ( temp[3] << 24 ) + ( temp[2] << 16 ) + ( temp[1] << 8 ) + temp[0];

            aFile.Seek( 14, SeekOrigin.Begin );
            aFile.Read( temp, 0, 4 );
            this.infoSize = ( temp[3] << 24 ) + ( temp[2] << 16 ) + ( temp[1] << 8 ) + temp[0];

            aFile.Seek( 18, SeekOrigin.Begin );
            aFile.Read( temp, 0, 4 );
            this.width = ( temp[3] << 24 ) + ( temp[2] << 16 ) + ( temp[1] << 8 ) + temp[0];
            image.Width = this.width;

            aFile.Seek( 22, SeekOrigin.Begin );
            aFile.Read( temp, 0, 4 );
            this.height = ( temp[3] << 24 ) + ( temp[2] << 16 ) + ( temp[1] << 8 ) + temp[0];
            image.Height = this.height;

            aFile.Seek( 28, SeekOrigin.Begin ); //image format must be 24bit
            aFile.Read( temp, 0, 2 );
            this.colorDepth = ( temp[1] << 8 ) + temp[0];
            if ( this.colorDepth != 24 )
            {
                Console.WriteLine( "Error: This is not a 24bit BMP!" );
                return null;
            }

            aFile.Seek( 30, SeekOrigin.Begin ); //compression is not allowed
            aFile.Read( temp, 0, 2 );
            this.compression = ( temp[1] << 8 ) + temp[0];
            if ( this.compression != 0 )
            {
                Console.WriteLine( "Error: This file uses compression!" );
                return null;
            }

            aFile.Seek( 34, SeekOrigin.Begin ); //image size including padding
            aFile.Read( temp, 0, 4 );
            this.imSize = ( temp[3] << 24 ) + ( temp[2] << 16 ) + ( temp[1] << 8 ) + temp[0];

            this.imBlock = new byte[this.imSize]; //image block including paddings
            aFile.Seek( this.imOffset, SeekOrigin.Begin );
            aFile.Read( this.imBlock, 0, this.imSize );

            this.padding = 0;
            if ( this.width % 4 != 0 )
            {
                this.padding = 4 - this.width % 4;
            }

            int i = 0;
            int x = 0, y = 0;
            while ( i < imSize )
            {
                Pixel p = new Pixel();
                p.Red = imBlock[i++];
                p.Green = imBlock[i++];
                p.Blue = imBlock[i++];
                p.X = x;
                p.Y = y;
                image.Pixels.Add( (x, y), p );
                x++;
                if ( x == width )
                {
                    x = 0;
                    y++;
                    i += padding;
                }
            }

            this.headerBlock = new byte[this.imOffset]; //header block
            aFile.Seek( 0, SeekOrigin.Begin );
            aFile.Read( this.headerBlock, 0, this.imOffset );

            aFile.Close();

            return image;
        }

        public void Write( string filePath, Image.Image image )
        {
            this.imSize = ( this.width + this.padding ) * this.height * 3;
            this.fileSize = this.imOffset + this.imSize; //header + pixelsize

            byte[] memBlock = new byte[this.fileSize];

            int l = 0;
            //header filled out with the original image
            for ( int i = 0; i < this.imOffset; i++ )
            {
                memBlock[l] = headerBlock[i];
                l++;
            }

            //overwrite the fileSize
            memBlock[2] = ( byte )( fileSize & 0xff );
            memBlock[3] = ( byte )( fileSize >> 8 & 0xff );
            memBlock[4] = ( byte )( fileSize >> 16 & 0xff );
            memBlock[5] = ( byte )( fileSize >> 24 & 0xff );

            //overwrite the imageSize
            memBlock[34] = ( byte )( imSize & 0xff );
            memBlock[35] = ( byte )( imSize >> 8 & 0xff );
            memBlock[36] = ( byte )( imSize >> 16 & 0xff );
            memBlock[37] = ( byte )( imSize >> 24 & 0xff );

            //image block filled out with the pixels
            for ( int y = 0; y < height; y++ )
            {
                for ( int x = 0; x < width; x++ )
                {
                    if ( colorDepth == 1 || colorDepth == 8 )
                    {
                        memBlock[l] = ( byte )( image.Pixels[(x, y)].Intensity );
                        l++;
                    }
                    else
                    {
                        memBlock[l] = ( byte )( image.Pixels[(x, y)].Red );
                        l++;
                        memBlock[l] = ( byte )( image.Pixels[(x, y)].Green );
                        l++;
                        memBlock[l] = ( byte )( image.Pixels[(x, y)].Blue );
                        l++;
                    }
                }
                for ( int p = 0; p < padding; p++ )
                {
                    memBlock[l] = 0;
                    l++;
                }
            }

            System.IO.File.WriteAllBytes( filePath, memBlock );
        }
    }
}
