﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LegoRecognition.Io.Image
{
    public class Image
    {
        private int wirth;
        private int height;

        public int Width
        {
            get { return wirth; }
            set { wirth = value; }
        }

        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        public Dictionary<(int, int), Pixel> Pixels { get; set; } = new Dictionary<(int, int), Pixel>();
    }
}
