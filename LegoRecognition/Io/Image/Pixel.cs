﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LegoRecognition.Io.Image
{
    public class Pixel
    {
        public int Red { get; set; } //1 byte
        public int Green { get; set; } //1 byte
        public int Blue { get; set; } //1 byte
        public int Intensity { get; set; } //1 byte
        public int X { get; set; }
        public int Y { get; set; }
    }
}
